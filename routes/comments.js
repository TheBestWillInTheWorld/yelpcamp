var express     = require('express'),
    router      = express.Router({mergeParams:true}),
    Campground  = require('../models/campground'),
    Comment     = require('../models/comment'),
    middleware  = require('../middleware');


//CREATE
router.get('/new', middleware.isLoggedIn, function(req, res){
    Campground.findById(req.params.id)
              .exec(function(err, item) {
                    if(err || !item){    
                        if(err) console.log(err)  ;     
                        req.flash("error","Campground not found."); 
                        res.redirect('back');
                    } 
                    else {      
                        res.render('comments/new',{campground:item});   
                    }
               });
});
router.post('/', middleware.isLoggedIn, function(req, res){
    Campground.findById(req.params.id)
              .exec(function(err, campground) {
                    if(err || !campground){    
                        if(err) console.log(err)  ;  
                        req.flash("error","Campground not found."); 
                        res.redirect('back');
                    } 
                    else {      
                        //add comment
                        Comment.create(req.body.comment,
                                function(err, comment) {
                                    if(err){    
                                        console.log(err);
                                        req.flash("error","Create comment failed."); 
                                        res.redirect('/campgrounds');
                                    } else {
                                        comment.author.id = req.user._id;
                                        comment.author.username = req.user.username;
                                        comment.save();
                                        campground.comments.push(comment);
                                        campground.save();
                                        console.log('added comment');
                                        req.flash("success","Successfully added comment."); 
                                        //show parent campground page
                                        res.redirect('/campgrounds/'+campground._id);
                                        //res.render('campgrounds/show',{campground:campground});
                                    }
                                });
                    }
               });
});
//VIEW

//UPDATE
router.get('/:comment_id/edit', middleware.isCommentAuthor, function(req, res){
    if (req.isAuthenticated){
        Comment.findById(req.params.comment_id, function(err, foundComment){
            if(err || !foundComment){    
                if(err) console.log(err)  ;  
                req.flash("error","Comment not found."); 
                res.redirect('back');
            }
            else {     
                Campground.findById(req.params.id, function(err, foundCampground){
                    if(err || !foundCampground){    
                        if(err) console.log(err)  ;  
                        res.redirect('back');
                    }
                    else {     
                        res.render('comments/edit', {campground: foundCampground,
                                                     campground_id: req.params.id, 
                                                     comment: foundComment});
                    }
                });
            }
        });
    } else {
        console.log('not logged in')
        req.flash("error","Please log in first.");
        res.redirect('back');
    }
});
router.put('/:comment_id', middleware.isCommentAuthor, function(req, res){
    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function(err, updatedComment){
        if(err || !updatedComment){    
            if(err) console.log(err)  ;  
            req.flash("error","Comment not found."); 
            res.redirect('back');
        }
        else {     
            req.flash("success","Comment updated."); 
            res.redirect('/campgrounds/'+req.params.id);
        }
        
    })
});
//DELETE
router.delete('/:comment_id', middleware.isCommentAuthor, function(req, res){
    Comment.findByIdAndRemove(req.params.comment_id, req.body.comment, function(err, updatedComment){
        if(err || !updatedComment){    
            if(err) console.log(err)  ;  
            req.flash("error","Comment not found."); 
            res.redirect('back');
        }
        else {     
            req.flash("success","Comment deleted."); 
            res.redirect('/campgrounds/'+req.params.id);
        }
        
    })
});



module.exports = router;