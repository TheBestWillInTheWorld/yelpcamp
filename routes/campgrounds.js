var express     = require('express'),
    router      = express.Router(),
    Campground  = require('../models/campground'),
    middleware  = require('../middleware');
    
//LIST
router.get('/', function(req, res){
    console.log("campgrounds");
    Campground.find({},function(err, objects){
        if(err || !objects){
            if(err) console.log(err)  ;  
            req.flash("error","No Campgrounds found."); 
            res.render('campgrounds/index');
        } else {
            res.render('campgrounds/index',{campgrounds:objects});
        }
    });
});

//CREATE
router.get('/new', middleware.isLoggedIn, function(req, res){
    res.render('campgrounds/new');
});
router.post('/', middleware.isLoggedIn, function(req, res){
    var name  = req.body.name;
    var image = req.body.image;
    var desc  = req.body.description;
    var price  = req.body.price;
    var author = {
        id: req.user._id,
        username: req.user.username
    };
    
    Campground.create({name: name, image: image, price: price, description: desc, author:author}, function(err, obj){
        if(err || !obj){
            if(err) console.log(err)  ;  
            req.flash("error","Campground not created."); 
            res.redirect('back');
        } else {
            console.log('added campground');
            console.log(obj);
            res.redirect('/campgrounds');
        }
    });
});

//VIEW
router.get('/:id', function(req, res){
    Campground.findById(req.params.id)
              .populate('comments')
              .exec(function(err, item) {
                if(err || !item){    
                    if(err) console.log(err)  ;  
                    req.flash("error","Campground not found."); 
                    res.redirect('back'); 
                }
                else {      
                    res.render('campgrounds/show',{campground:item});   
                }
               });
});

//UPDATE
router.get('/:id/edit', middleware.isCampgroundAuthor, function(req, res){
    if (req.isAuthenticated){
        Campground.findById(req.params.id, function(err, foundCampground){
            if(err || !foundCampground){    
                if(err) console.log(err)  ;  
                req.flash("error","Campground not found."); 
                res.redirect('back');
            }
            else {     
                res.render('campgrounds/edit', {campground: foundCampground});
            }
        });
    } else {
        console.log('not logged in')
    }
});
router.put('/:id', middleware.isCampgroundAuthor, function(req, res){
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground){
        if(err || !updatedCampground){    
            if(err) console.log(err)  ; 
            req.flash("error","Campground not found."); 
            res.redirect('/campgrounds');
        }
        else {     
            req.flash("success","Campground updated."); 
            res.redirect('/campgrounds/'+ req.params.id);
        }
        
    })
});
//DELETE
router.delete('/:id', middleware.isCampgroundAuthor, function(req, res){
    Campground.findByIdAndRemove(req.params.id, req.body.campground, function(err, updatedCampground){
        if(err || !updatedCampground){    
            if(err) console.log(err)  ; 
            req.flash("error","Campground not found."); 
            res.redirect('/campgrounds');
        }
        else {     
            req.flash("success","Campground deleted."); 
            res.redirect('/campgrounds');
        }
        
    })
});


module.exports = router;