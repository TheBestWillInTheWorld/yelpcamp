var express     = require('express'),
    router      = express.Router(),
    User        = require("../models/user"),    
    passport    = require("passport");
/****************************/
/*      Auth Routes         */
/****************************/

router.get("/register", function(req, res){
   res.render("register"); 
});
//handling user sign up
router.post("/register", function(req, res){
    User.register(new User({username: req.body.username}), req.body.password, 
        function(err, user){
            if(err){
                console.log(err);
                req.flash("error", err.message);
                res.redirect('/register');
            } else {
                //auto-login
                passport.authenticate("local")(req, res, function(){
                  req.flash("success", "Welcome to YelpCamp"+ user.username +'!');
                  res.redirect("/campgrounds");
                });
            }
        });
});
router.get("/login", function(req, res){
   res.render("login"); 
});
router.post("/login", passport.authenticate("local", {
                        successRedirect: "/campgrounds",
                        failureRedirect: "/login"
                    }) ,
                    function(req, res){
})
router.get("/logout", function(req, res){
    req.logout();
    res.redirect("/");
});
  
/****************************/
/*      Root Routes         */
/****************************/

router.get('/', function(req, res){
    res.render('landing');
});

/****************************/

module.exports = router;