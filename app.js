var express     = require('express'),
    app         = express(),
    bodyParser  = require('body-parser'),
    mongoose    = require('mongoose'),
    Campground  = require('./models/campground'),
    Comment     = require('./models/comment'),
    seedDB      = require('./seeds'),    
    passport              = require("passport"),
    User                  = require("./models/user"),
    LocalStrategy         = require("passport-local"),
    passportLocalMongoose = require("passport-local-mongoose"),
    methodOverride        = require("method-override"),
    flash                 = require("connect-flash")

var Cleanup = require('./utils/cleanup');

//requring routes
var commentRoutes    = require("./routes/comments"),
    campgroundRoutes = require("./routes/campgrounds"),
    indexRoutes      = require("./routes/index")
  

app.use(express.static(__dirname + '/public'));
app.use(methodOverride('_method'));
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');

//mongoose compatiblity
mongoose.Promise = global.Promise; 
mongoose.connect(process.env.MONGO_URL, {useMongoClient: true});

//session & auth
app.use(
    require("express-session")({
        secret: process.env.EXPRESS_SESSION_SECRET,
        resave: false,
        saveUninitialized: false
    })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser())

app.use(function(req, res, next) {
    console.log(req.url);
    res.locals.currentUser = req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    next();
})
    
// seedDB(false);
  
/****************************/

app.use('/',                            indexRoutes);
app.use('/campgrounds',                 campgroundRoutes);
app.use('/campgrounds/:id/comments',    commentRoutes);

app.get('*', function(req, res){
    res.send('404');
});

/****************************/
app.listen(process.env.PORT, process.env.IP, function(){
    console.log('YelpCamp Server started at '+ process.env.IP +':'+process.env.PORT);
});



Cleanup.setupExitHandling(function(options, err){
    //close DB connection to prevent conneciton leaks (c9 seems to fail more if connecitons are leaked)
    mongoose.connection.close();
    console.log('cleanup');
    //TODO: custom cleanup here
})