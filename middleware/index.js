var express     = require('express'),
    app         = express(),
    Campground  = require('../models/campground'),
    Comment     = require('../models/comment')

module.exports = {
    
    isCampgroundAuthor: function (req, res, next){
        if(req.isAuthenticated()){
            Campground.findById(req.params.id, function(err, foundCampground){
                if(err || !foundCampground){    
                    if(err) console.log(err)  ;
                    req.flash("error","Campground not found.");  
                    res.redirect('back');
                }
                else if(foundCampground.author.id.equals(req.user._id)){
                    next();
                } else {
                    req.flash("error","Access denied.");  
                }
            });
        } else {
            req.flash("error","Please log in first.");
            res.redirect("back");
        }
    },
    
    isCommentAuthor: function (req, res, next){
        if(req.isAuthenticated()){
            Comment.findById(req.params.comment_id, function(err, foundComment){
                if(err || !foundComment){    
                    if(err) console.log(err)  ;
                    req.flash("error","Comment not found.");  
                    res.redirect('back');
                }
                else if(foundComment.author.id.equals(req.user._id)){
                    next();
                } else {
                    req.flash("error","Access denied.");  
                }
            });
        } else {
            req.flash("error","Please log in first.");
            res.redirect("back");
        }
    },
    
    isLoggedIn: function (req, res, next){
        if(req.isAuthenticated()){
            return next();
        }
        req.flash("error","Please log in first.");
        res.redirect("/login");
    }
    
}