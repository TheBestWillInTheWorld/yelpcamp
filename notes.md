 line of code into your app, after you've required mongoose: mongoose.Promise = global.Promise; 

That line of code simply replaces Mongoose's default promise library with JavaScript's native promise library.

Update: Another warning that you may experience when using mongoose is this:

`open()` is deprecated in mongoose >= 4.11.0, use `openUri()` instead, 
or set the `useMongoClient` option if using `connect()` or `createConnection()`
To make this go away simply use:

mongoose.connect("mongodb://localhost/yelp_camp", {useMongoClient: true});




```js
//lorempixel.com is slow
//https://picsum.photos/210/161 is quicker
    // Campground.create( {name: 'place' , image:'http://lorempixel.com/210/161'},handle);
    // Campground.create( {name: 'place2', image:'http://lorempixel.com/210/162'},handle);
    // Campground.create( {name: 'place3', image:'http://lorempixel.com/210/163'},handle);
    // Campground.create( {name: 'place4', image:'http://lorempixel.com/210/164'},handle);
    
    // function handle(err, obj) {
    //     if(err){
    //         console.log("error")
    //     } else {
    //         console.log('saved')
    //     }
    // }
```


nodemon (available on npm) can be used in place of [node] for running an app.
It auto-relaunches a node app upon cahnge to any .js files.